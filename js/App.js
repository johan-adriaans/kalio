import dom from './Dom-minified.js';

/**
 * Project page logic
 */
class Project {
  constructor(project_element) {
    this.wait_after_scroll = 300;

    this.project_element = project_element;
    this.scroll_container = project_element.find('.pages');
    this.page_count = project_element.find('.pages > .page').length() - 1;
    this.page_cursor = 0;

    this.refreshWidth();

    this.scroll_timeout = false;
    this.scroll_container.on('scroll', () => this.resetAfterScroll());

    // Reset pages after resize
    window.addEventListener('resize', () => {
      this.refreshWidth();
      this.scrollToPage(this.page_cursor, false);
    });

    this.renderPagination();
    this.scrollToPage(0);
  }

  refreshWidth() {
    this.project_width = this.scroll_container.width();
  }

  renderPagination() {
    this.previous_button = dom('<button>')
      .addClass('previous')
      .text('previous page')
      .text('previous page')
      .on('click', () => this.previousPage())
      .appendTo(this.project_element);

    this.next_button = dom('<button>')
      .addClass('next')
      .text('next page')
      .on('click', () => this.nextPage())
      .appendTo(this.project_element);

    this.page_index_container = dom('<div>')
      .addClass('page-index')
      .appendTo(this.project_element);

    this.index_buttons = [];
    for (let i = 0; i <= this.page_count; i++) {
      this.index_buttons[i] = dom('<button>')
        .text(i + 1)
        .on('click', () => this.scrollToPage(i))
        .appendTo(this.page_index_container);
    }
  }

  nextPage() {
    this.scrollToPage(this.page_cursor + 1);
  }

  previousPage() {
    this.scrollToPage(this.page_cursor - 1);
  }

  resetAfterScroll() {
    if (this.scroll_timeout) clearTimeout(this.scroll_timeout);
    this.scroll_timeout = setTimeout(() => {
      const target_page = Math.round(this.scroll_container.prop('scrollLeft') / this.project_width);
      this.scrollToPage(target_page);
    }, this.wait_after_scroll);
  }

  scrollToPage(page, smooth = true) {
    page = Math.max(0, Math.min(page, this.page_count));
    const scroll_target = page * this.project_width;
    this.scroll_container.elements[0].scroll({
      top: 0,
      left: scroll_target,
      behavior: smooth ? 'smooth' : 'auto'
    });
    this.page_cursor = page;

    // Enable/disable buttons based on current page
    this.previous_button.attr('disabled', page == 0 ? true : null);
    this.next_button.attr('disabled', page == this.page_count ? true : null);
    this.index_buttons.forEach((button, i) => {
      button.attr('disabled', i == page ? true : null);
    });
  }
}

// Initiate all projects
const projects = dom('#projects .project');
projects.each(project => {
  new Project(project);
});

/**
 * Parallax effect behind logo
 */
const parallaxElement = dom('#home .header');
let timerStep = 0;
setInterval(() => {
  const ratios = [1, 0.9, 0, 0.5, 0.4, 0];
  const step = Math.cos(timerStep) * 50;
  const position_data = [];

  ratios.forEach(r => {
    position_data.push((50 + r * step).toString() + '% 50%');
  });
  parallaxElement.css('background-position', position_data.join(','));
  timerStep += 0.003;
}, 20);

/**
 * Teaser logic
 */
/*
const split_element = dom('.slogan-split');
split_element.on('click', () => {
  flash_teaser();
});

// Show once while scolling
let teaser_shown = false;
window.addEventListener('scroll', () => {
  if (!teaser_shown && window.scrollY > 200) {
    flash_teaser();
    teaser_shown = true;
  }
});

// Teaser toggle function
let teaser_timeout = false;
function flash_teaser() {
  const teaser_top_open = window.innerWidth < 900 ? '-10vw' : '-90px';
  const teaser_top_closed = window.innerWidth < 900 ? '-49vw' : '-441px';
  if (teaser_timeout) clearTimeout(teaser_timeout);
  split_element.css('margin-top', teaser_top_open);
  setTimeout(() => {
    split_element.css('margin-top', teaser_top_closed);
    teaser_timeout = false;
  }, 2000);
}
*/
